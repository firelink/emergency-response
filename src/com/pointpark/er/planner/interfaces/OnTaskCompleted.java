package com.pointpark.er.planner.interfaces;

public interface OnTaskCompleted
{

	void onTaskCompleted(String value);
}
