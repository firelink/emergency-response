package com.pointpark.er.planner;

import java.util.HashMap;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TabHost;
import android.widget.TabHost.TabContentFactory;

public class TabMenu extends FragmentActivity implements TabHost.OnTabChangeListener 
{
	private TabHost mTabHost;
	private HashMap<String, TabInfo> mapTabInfo = new HashMap<String, TabInfo>();
	private TabInfo mLastTab = null;
	private Bundle b;
	public static final String PREFS = "LotteryAid.prefs";
	private int requ = 0;

	public class TabInfo 
	{
		private String tag;
		private Class<?> clss;
		private Bundle args;
		private Fragment fragment;

		TabInfo(String tag, Class<?> clazz, Bundle args) 
		{
			this.tag = tag;
			this.clss = clazz;
			this.args = args;
		}
	}

	class TabFactory implements TabContentFactory 
	{
		private final Context mContext;

		public TabFactory(Context context) 
		{
			mContext = context;
		}

		public View createTabContent(String tag) 
		{
			View v = new View(mContext);
			v.setMinimumWidth(0);
			v.setMinimumHeight(0);
			return v;
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		if(requ == 0)
		{
			this.requestWindowFeature(Window.FEATURE_NO_TITLE);
			requ++;
		}
		
		if(getResources().getBoolean(R.bool.portrait_only)){
	        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    }
		
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.tab_main);
		//initOnCreate(savedInstanceState);
		
		initTabHost(savedInstanceState);
		if (savedInstanceState != null) 
		{
			mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab"));
		}
		
	}
	
	public void onResumeFragments()
	{
		super.onResumeFragments();
		
	}

	protected void onSaveInstanceState(Bundle out) 
	{
		out.putString("tab", mTabHost.getCurrentTabTag());
		super.onSaveInstanceState(out);
	}
	
	public boolean onCreateOptionsMenu(Menu menu)
	{
		//MenuInflater inflate = getMenuInflater();
		//inflate.inflate(R.menu.main_menu, menu);

		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item)
	{
		return true;
	}
	
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		
		setContentView(R.layout.tab_main);

		initTabHost(b);
		if (b != null) 
		{
			mTabHost.setCurrentTabByTag(b.getString("tab"));
		}
		
	}

	private void initTabHost(Bundle args) 
	{
		mTabHost = (TabHost)findViewById(android.R.id.tabhost);
		mTabHost.setup();
		TabInfo tabInfo = null;
		
		TabMenu.addTab(this, this.mTabHost, this.mTabHost
				.newTabSpec("Home").setIndicator("Home"),
				(tabInfo = new TabInfo("Home", HomeActivity.class, null)));
		this.mapTabInfo.put(tabInfo.tag, tabInfo);
		
		TabMenu.addTab(this, this.mTabHost, this.mTabHost
				.newTabSpec("Scan Code").setIndicator("Scan Code"),
				(tabInfo = new TabInfo("Scan Code", ScannerActivity.class, null)));
		this.mapTabInfo.put(tabInfo.tag, tabInfo);
		
		TabMenu.addTab(this, this.mTabHost, this.mTabHost
					   .newTabSpec("Search For Item").setIndicator("Search For Item"),
					   (tabInfo = new TabInfo("Search For Item", null, null)));
		this.mapTabInfo.put(tabInfo.tag, tabInfo);
		
		
		
		// Default to first tab
		this.onTabChanged("Home");

		mTabHost.setOnTabChangedListener(this);
	}
	
	private static void addTab(TabMenu activity, TabHost tabHost, TabHost.TabSpec tabSpec, TabInfo tabInfo)
	{
		tabSpec.setContent(activity.new TabFactory(activity));
		String tag = tabSpec.getTag();
		
		tabInfo.fragment = activity.getSupportFragmentManager().findFragmentByTag(tag);
		if(tabInfo.fragment != null && !tabInfo.fragment.isDetached())
		{
			FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
			ft.detach(tabInfo.fragment);
			ft.commit();
			activity.getSupportFragmentManager().executePendingTransactions();
		}
		
		tabHost.addTab(tabSpec);
	}

	public void onTabChanged(String tag) 
	{
		
			TabInfo newTab = (TabInfo) this.mapTabInfo.get(tag);
			
		
			if(mLastTab != newTab)
			{
				FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
				ft.setCustomAnimations(R.anim.zoom_enter, 0);
			
				if(mLastTab != null)
				{
					if(mLastTab.fragment != null)
					{
						ft.detach(mLastTab.fragment);
					}
				}
			
				if(newTab != null)
				{
					if(newTab.fragment == null)
					{
						newTab.fragment = Fragment.instantiate(this, newTab.clss.getName(), newTab.args);
						
						//newTab.fragment.setArguments(bundleGame1);
						
						ft.add(R.id.realtabcontent, newTab.fragment, newTab.tag);
					
					}
					else
					{
						ft.attach(newTab.fragment);
					}
				}
			
				mLastTab = newTab;
				ft.commit();
				this.getSupportFragmentManager().executePendingTransactions();	
			}
	}
}
