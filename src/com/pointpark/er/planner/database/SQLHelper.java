package com.pointpark.er.planner.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.pointpark.er.planner.interfaces.*;

/*
 * Helper function used to make database queries more streamlined and efficient.
 */

public class SQLHelper extends AsyncTask<Void,Void,String> 
{
	public static final int UPDATE = 0;
	public static final int INSERT = 1;
	public static final int SELECT = 3;
	private String query;
	private String userName;
	private String password;
	private String URL;
	private String[] fields;
	private int dbFunction;
	private String results;
	private Context context;
	private OnTaskCompleted mListener;

	public SQLHelper()
	{
		this("", "root", "admin", "localhost", 3);
	}

	SQLHelper(String query, String userName, String password, String URL, int dbFunction)
	{
		this.query = query;
		this.userName = userName;
		this.password = password;
		this.URL = URL;
		this.dbFunction = dbFunction;

		this.results = "Blah";
	}

	//Getters
	public void setQuery(String query){
		this.query = query;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public void setURL(String URL){
		this.URL = URL;
	}

	public void setDBFunction(int dbFunction){
		this.dbFunction = dbFunction;
	}

	public void setContext(Context context){
		this.context = context;
	}

	public void setFields(String[] fields)
	{
		this.fields = fields;
	}

	//Getters
	public String getResults(){
		return results;
	}

	public void setListener(OnTaskCompleted listener){
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(Void... params) 
	{
		this.results = "Failure";
		try 
		{

			Class.forName("org.postgresql.Driver");

			DriverManager.setLoginTimeout(5);
			Connection conn = DriverManager.getConnection(URL, userName, password);
			Statement state = conn.createStatement();
			//String sql2 = "INSERT into android.users(firstname, lastname, email) VALUES('Jesse', 'Padjune', 'jtpadju@pointpark.edu')";
			//st.executeUpdate(sql2);
			switch(dbFunction)
			{
				case SELECT:
					ResultSet results = state.executeQuery(query);

					while (results.next()) 
					{
						this.results = "";
						for(String i : fields)
							this.results += ((results.getString(i) == null)? i:results.getString(i)) + "!!@@##";
					}

					results.close();
					break;

				case UPDATE:
					/*				query = "UPDATE android.codes " +
					 "SET description = '" + dbEditText.getText().toString() + "' " +
					 "WHERE code = '" + upc + "' ";
					 */
					int count = state.executeUpdate(query);
					if(count > 0){this.results = "Success!";}
					else{this.results = "Failed to update to database";}

					break;
				case INSERT:
					/*				query =   "INSERT into android.codes " +
					 "VALUES('" + upc + "', '" + dbEditText.getText().toString() + "')";
					 */
					int count2 = state.executeUpdate(query);
					if(count2 > 0){this.results = "Success!";}
					else{this.results = "Failed to add entry";}

					break;
			}


			state.close();
			conn.close();

			return this.results;


		}
		catch (Exception e) 
		{
			Log.e("AndroidRuntime", "Error", e); 
			e.printStackTrace();
			this.results = e.toString();
		}

		return this.results;

	}

	@Override
	protected void onPostExecute(String value) 
	{
		Log.d("ER Planner SQL", value);
		if(dbFunction == this.INSERT || dbFunction == this.UPDATE)value = "Success";
		mListener.onTaskCompleted(value);

		//resultArea.setText(value);
	}

}
