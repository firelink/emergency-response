package com.pointpark.er.planner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pointpark.er.planner.glue.Glue;

public class LoginActivity extends Activity {
	// fields
	private static final String USER_PREFS = "UserPreferences";
	//Login Fields
	private EditText loginEditTextFirstName;
	private EditText loginEditTextLastName;
	private Button loginButton;
	private TextView loginDisplay;
	private Context context;
	// private OnTaskCompleted mListener;
	private SharedPreferences prefs;


	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if(getResources().getBoolean(R.bool.portrait_only)){
	        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    }
		
		prefs = this.getSharedPreferences(USER_PREFS, 0);
		if (prefs.contains("firstName")) {
			Intent myIntent = new Intent(this, TabMenu.class);
			startActivity(myIntent);
			finish();
		}

		setContentView(R.layout.login);

		initializeLogin();


		context = this;

	}

	/** Initializes the components related to the layout R.layout.login */
	private void initializeLogin() {
		// Initialize view components
		loginEditTextFirstName = (EditText) findViewById(R.id.login_editTextLoginUsername);
		loginEditTextFirstName
				.setOnFocusChangeListener(new View.OnFocusChangeListener() {
					public void onFocusChange(View p1, boolean p2) {
						String textString = loginEditTextFirstName.getText()
								.toString();

						if (textString.equals("First name")) {
							loginEditTextFirstName.setText("");
						} else if (textString.equals("")) {
							loginEditTextFirstName.setText("First name");
						}
					}
				});

		loginEditTextLastName = (EditText) findViewById(R.id.login_editTextLoginPassword);
		loginEditTextLastName
				.setOnFocusChangeListener(new View.OnFocusChangeListener() {
					public void onFocusChange(View p1, boolean p2) {
						String textString = loginEditTextLastName.getText()
								.toString();

						if (textString.equals("Last name")) {
							loginEditTextLastName.setText("");
						} else if (textString.equals("")) {
							loginEditTextLastName.setText("Last name");
						}
					}
				});

		loginDisplay = (TextView) findViewById(R.id.login_textViewLoginReturnDisplay);

		loginButton = (Button) findViewById(R.id.login_buttonLogin);
		loginButton.setOnClickListener(loginListener);
	}
	
	/**Button click listener: loginListener */
	public View.OnClickListener loginListener = new View.OnClickListener() {
		public void onClick(View p1) {
			boolean testLogin;
			testLogin = Glue.userLogin(loginEditTextFirstName.getText().toString(),
					loginEditTextLastName.getText().toString(), context);
			if (testLogin) {
				loginDisplay.setText("Logged in");
				
				loginEditTextFirstName.setOnFocusChangeListener(null);
				loginEditTextFirstName = null;
				loginEditTextLastName.setOnFocusChangeListener(null);
				loginEditTextLastName = null;
				loginButton = null;
				loginDisplay = null;
				
				setContentView(R.layout.home);
				Intent myIntent = new Intent(LoginActivity.this, TabMenu.class);
				startActivity(myIntent);
				finish();
				
			
			} else {
				loginDisplay.setText("You forgot a field or two...");
			}
		}
	};
}
//End of class
