package com.pointpark.er.planner;

import java.util.List;

import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.pointpark.er.planner.Scanner.history.*;

public class HomeActivity extends Fragment {
	// fields
	private static final String USER_PREFS = "UserPreferences";
	//Main fields
	private TextView mainTextViewTest;
	// private OnTaskCompleted mListener;
	private SharedPreferences prefs;
	
	
	
	/** Called when the activity is first created. */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		
		LinearLayout theLayout = (LinearLayout) inflater.inflate(R.layout.home, container, false);
		
		prefs = getActivity().getSharedPreferences(USER_PREFS, 0);
		
		initializeMain(theLayout);
		
		return theLayout;
		
	};
	
	/** Initializes the components related to the layout R.layout.main  */
	private void initializeMain(LinearLayout theLayout) {
		// Initialize view components
		// Do something
		mainTextViewTest = (TextView)theLayout.findViewById(R.id.main_testDisplay);
		
		String displayName = "Welcome, ";
		displayName += prefs.getString("firstName", "First Name");
		displayName += " "; 
		displayName += (prefs.getString("lastName", "Last Name").substring(0, 1)) + ".";
		
		mainTextViewTest.setText(displayName);
	
		
	}
}
//End of class
