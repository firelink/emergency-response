package com.pointpark.er.planner.glue;
import android.content.*;
import android.preference.PreferenceManager;

import com.pointpark.er.planner.database.*;
import com.pointpark.er.planner.interfaces.*;
import java.security.*;


public final class Glue
{
	private static final String USER_PREFERENCE_FILE = "UserPreferences";

	public static String MD5(String input) 
	{
		MessageDigest digest;
		
		try {
			digest = MessageDigest.getInstance("MD5");
			digest.reset();
			digest.update(input.getBytes());
			
			byte[] aByte = digest.digest();
			int len = aByte.length;
			StringBuilder output = new StringBuilder(len << 1);
			
			for (int i = 0; i < len; i++) 
			{
				output.append(Character.forDigit((aByte[i] & 0xf0) >> 4, 16));
				output.append(Character.forDigit(aByte[i] & 0x0f, 16));
			}
			
			return output.toString();
			
		} catch (NoSuchAlgorithmException e) 
		{ 
			e.printStackTrace(); 
		}
		
		return null;
	}
	
	public static boolean userLogin(String firstName, String lastName, Context context)
	{
		SharedPreferences prefs = context.getSharedPreferences(USER_PREFERENCE_FILE, 0);
		SharedPreferences.Editor pEditor = prefs.edit();
		
		if(firstName.equals("")){
			return false;
		} else if (lastName.equals("")){
			return false;
		} else if (firstName.equals("First name")){
			return false;
		} else if (lastName.equals("Last name")){
			return false;
		}
		
		
		pEditor.putString("firstName", firstName);
		pEditor.putString("lastName", lastName);
		pEditor.commit();
		
		
		
		return true;
		/*
		SQLHelper dbHelp = new SQLHelper();
		String query = "SELECT * "
			+ "FROM android_dev.tblUsers "
			+ "WHERE userName = '" + username + "' AND "
			+ "	  userPassword = md5('" + password + "');";

		dbHelp.setQuery(query);
		dbHelp.setFields(new String[] { "userID", "firstName", "lastName", "email" });
		dbHelp.setDBFunction(SQLHelper.SELECT);
		dbHelp.setURL("jdbc:postgresql://bus1.pointpark.edu/jsmith");
		dbHelp.setUserName("jsmith");
		dbHelp.setPassword("android");
		dbHelp.setContext(context);
		dbHelp.setListener(mListener);
		dbHelp.execute();
		*/
	}
}
