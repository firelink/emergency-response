package com.pointpark.er.planner;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pointpark.er.planner.Scanner.CaptureActivity;

public class ScannerActivity extends Fragment
{
	
	public static final int UPDATE = 0;
	public static final int INSERT = 1;
	
	private LinearLayout theLayout;
	private TextView textResultArea;
	private TextView textResults;

	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
        super.onCreateView(inflater, container, savedInstanceState);
        container.removeAllViews();
        theLayout = (LinearLayout)inflater.inflate(R.layout.scanner, container, false);

		textResultArea = (TextView)theLayout.findViewById(R.id.scannerTextViewResult);
		textResults = (TextView)theLayout.findViewById(R.id.scannerResults);
		textResults.setVisibility(View.GONE);
		
		
		Button btn = (Button)theLayout.findViewById(R.id.dataButton);
		btn.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View p1)
			{
				//IntentIntegratorV30 intent = new IntentIntegratorV30(frag);
				//intent.initiateScan();
				//Intent intent = new Intent(getActivity(), CaptureActivity.class);
				//startActivityForResult(intent, 0);
				Intent intent = new Intent(getActivity(), CaptureActivity.class);
				startActivityForResult(intent, 0);
			}
		});
		
		return theLayout;
    }
	
    public void onActivityResult(int requestCode, int resultCode, Intent intent) 
    {
       if (requestCode == 0) {
          if (resultCode == Activity.RESULT_OK) 
          {
        	 String upc = "Barcode: " + intent.getStringExtra("SCAN_RESULT");
    	    	 
    	     //dbEditText.setVisibility(View.VISIBLE);
    	     //dbTextView.setVisibility(View.VISIBLE);
    	     textResultArea.setText(upc);
          } 
          else if (resultCode == Activity.RESULT_CANCELED) 
          {
        	  
          }
       }
    }
}

